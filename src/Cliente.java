import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;


public class Cliente {
	
	public static boolean ValidaResposta(String resposta) {
		if (resposta.equals("1") ||resposta.equals("2")
				|| resposta.equals("3")|| resposta.equals("5")
				|| resposta.equals("4")){
			return true;
		} else
			return false;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String resposta = "";
		try {
			System.out.println("=== Pedra, Papel, Tesoura, Lagarto e Spock ===");
			Registry registry = LocateRegistry.getRegistry();
			Servidor servidorJogo = (Servidor) registry.lookup("servidorJogo");
			System.out.print("Informe seu nome: ");
			String nome = scanner.nextLine();
			servidorJogo.registrarJogador(nome);
			
			while(!servidorJogo.fimPartida()){
				if(servidorJogo.rodadaAtiva()){
					System.out.print(servidorJogo.getOpcoes());
					System.out.print(nome + " digite a op��o escolhida: ");
					resposta = scanner.nextLine();
					while(!ValidaResposta(resposta)){
						System.out.println("Opcao invalida, digite novamente");
						resposta = scanner.nextLine();
					}
					servidorJogo.informarEscolha(Integer.valueOf(resposta), nome);
					
					if(servidorJogo.vencedorRodadaDisponivel()){
						System.out.print(servidorJogo.getVencedorRodada());
					}
				}
				else{
					//if(servidorJogo.vencedorRodadaDisponivel()){
						//System.out.print(servidorJogo.getVencedorRodada());
					//}
				}
			}
			System.out.print(servidorJogo.getVencedorPartida());
			System.exit(0);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}finally{
			scanner.close();
		}
	}
}
