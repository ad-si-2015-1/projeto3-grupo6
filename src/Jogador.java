import java.io.Serializable;

public class Jogador implements Serializable {
	private static final long serialVersionUID = -6063687936593155248L;
	private String escolha;
	private String nome;
	private Integer pontuacao;
	
	public Jogador(){
		pontuacao = 0;
		escolha = null;
	}

	public String getEscolha() {
		return escolha;
	}

	public void setEscolha(String escolha) {
		this.escolha = escolha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
}
