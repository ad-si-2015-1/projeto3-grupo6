import java.rmi.Remote;
import java.rmi.RemoteException;


public interface Servidor extends Remote {
	public String registrarJogador(String nome) throws RemoteException;
	public Boolean rodadaAtiva() throws RemoteException;
	public String getOpcoes() throws RemoteException;
	public void informarEscolha(Integer escolha, String nome) throws RemoteException;
	public Boolean vencedorRodadaDisponivel() throws RemoteException;
	public String getVencedorRodada() throws RemoteException;
	public Boolean fimPartida() throws RemoteException;
	public String getVencedorPartida() throws RemoteException;
}
