import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class ServidorImpl implements Servidor {
	
	private Jogador jogadorUm;
	private Jogador jogadorDois;
	private Boolean rodadaAtiva;
	private Boolean fimPartida;
	private Integer rodada;
	private Jogador jogador;
	
	public ServidorImpl() {
		rodadaAtiva = false;
		fimPartida = false;
		rodada = 1;
	}

	@Override
	public String registrarJogador(String nome) throws RemoteException {
		if(jogadorUm == null){
			jogadorUm = new Jogador();
			jogadorUm.setNome(nome);
			return "Bem vindo ao jogo Player 1";
		}else if(jogadorDois == null){
			jogadorDois = new Jogador();
			jogadorDois.setNome(nome);
			rodadaAtiva = true;
			return "Bem vindo ao jogo player 2\n\r Jogo iniciando!!";
		}else{
			return "Servidor cheio, tente novamente mais tarde";
		}
	}

	@Override
	public Boolean rodadaAtiva() throws RemoteException {
		return rodadaAtiva;
	}

	@Override
	public String getOpcoes() throws RemoteException {
		String opcoes = "\r\nRODADA " + rodada +"!!\r\n\r\n"
				+ "1-Pedra "
				+ "2-Papel "
				+ "3-Tesoura "
				+ "4-Lagarto "
				+ "5-Spock\r\n\r\n";
		return opcoes;
	}

	@Override
	public void informarEscolha(Integer escolha, String nome) throws RemoteException {
		if(nome.equals(jogadorUm.getNome()))
			jogador = jogadorUm;
		else
			jogador = jogadorDois;
		switch(escolha){
		case 1:
			jogador.setEscolha("pedra");
			break;
		case 2:
			jogador.setEscolha("papel");
			break;
		case 3:
			jogador.setEscolha("tesoura");
			break;
		case 4:
			jogador.setEscolha("lagarto");
			break;
		case 5:
			jogador.setEscolha("spock");
			break;
		}
		rodadaAtiva = false;
	}

	@Override
	public Boolean vencedorRodadaDisponivel() throws RemoteException {
		return jogadorUm.getEscolha() != null && jogadorDois.getEscolha() != null;
	}
	
	@Override
	public String getVencedorRodada() throws RemoteException {
		rodada++;
		String nomeVencedor = null;
		if(jogadorUm.getEscolha() == jogadorDois.getEscolha()){
			return "Ambos escolheram a mesma opcaoo, empate!!\n\r";
		}else{
			switch(jogadorUm.getEscolha().trim()){
			case "tesoura":
				if(jogadorDois.getEscolha() == "papel" || jogadorDois.getEscolha() == "lagarto"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor = jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor = jogadorDois.getNome();
				}
				break;
			case "spock":
				if(jogadorDois.getEscolha() == "tesoura" || jogadorDois.getEscolha() == "pedra"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor=jogadorDois.getNome();
				}
				break;
			case "pedra":
				if(jogadorDois.getEscolha() == "tesoura" || jogadorDois.getEscolha() == "lagarto"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor=jogadorDois.getNome();
				}
				break;
			case "lagarto":
				if(jogadorDois.getEscolha() == "papel" || jogadorDois.getEscolha() == "spock"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor=jogadorDois.getNome();
				}
				break;
			case "papel":
				if(jogadorDois.getEscolha() == "pedra" || jogadorDois.getEscolha() == "spock"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor = jogadorDois.getNome();
				}
				break;
			}
		}
		if(jogadorUm.getPontuacao() > 2 || jogadorDois.getPontuacao() > 2){
			fimPartida = true;
		}else{
			rodadaAtiva = true;			
		}
		jogadorUm.setEscolha(null);
		jogadorDois.setEscolha(null);
		return String.format("\n%s venceu essa rodada!!\n\rPlacar: %s %d X %d %s\r\nIniciando rodada %d\n\n\r",
				nomeVencedor, jogadorUm.getNome().toUpperCase(), jogadorUm.getPontuacao(),
				jogadorDois.getPontuacao(), jogadorDois.getNome().toUpperCase(), rodada);
	}

	@Override
	public Boolean fimPartida() throws RemoteException {
		return fimPartida;
	}

	@Override
	public String getVencedorPartida() throws RemoteException {
		String vencedor = "";
		
		if (jogadorUm.getPontuacao() > jogadorDois.getPontuacao())
			vencedor = jogadorUm.getNome();
		else
			vencedor = jogadorDois.getNome();
		
		return String.format("\n\r%s VENCEU A PARTIDA!!\n\rPlacar final: %s %d X %d %s\r\nFim de jogo!\n\r"
			+ "Parabens!! %s\n\n\r",
			vencedor.toUpperCase(), jogadorUm.getNome().toUpperCase(), jogadorUm.getPontuacao(),
			jogadorDois.getPontuacao(), jogadorDois.getNome().toUpperCase(), vencedor.toUpperCase());
	}
	
	public static void main(String[] args) {
		try {
			Servidor servidor = new ServidorImpl();
			Servidor servidorStub = (Servidor) UnicastRemoteObject.exportObject(servidor, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("servidorJogo", servidorStub);
		} catch (RemoteException e) {
			e.printStackTrace();
		} 
	}
}